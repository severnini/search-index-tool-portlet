package br.com.severnini.portlet;

import com.liferay.portal.model.Group;
import com.liferay.portal.model.Portlet;
import com.liferay.portal.security.permission.PermissionChecker;
import com.liferay.portlet.BaseControlPanelEntry;


/**
 * Control panel entry class SearchIndexToolPortletControlPanelEntry
 */
public class SearchIndexToolPortletControlPanelEntry extends BaseControlPanelEntry {

	@Override
	protected boolean hasPermissionImplicitlyGranted(
			PermissionChecker permissionChecker, Group group, Portlet portlet)
					throws Exception {
		return permissionChecker.isCompanyAdmin();
	}

}