package br.com.severnini.portlet;

import com.liferay.portal.kernel.dao.search.SearchContainer;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.language.LanguageUtil;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.search.Document;
import com.liferay.portal.kernel.search.Hits;
import com.liferay.portal.kernel.search.Query;
import com.liferay.portal.kernel.search.SearchContext;
import com.liferay.portal.kernel.search.SearchEngineUtil;
import com.liferay.portal.kernel.search.StringQueryFactoryUtil;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.portal.util.PortalUtil;
import com.liferay.util.bridges.mvc.MVCPortlet;

import java.io.IOException;
import java.util.Collections;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletException;
import javax.portlet.PortletRequest;
import javax.portlet.PortletResponse;
import javax.portlet.PortletURL;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

/**
 * Portlet implementation class SearchIndexToolPortlet
 */
public class SearchIndexToolPortlet extends MVCPortlet {

	@Override
	public void doView(RenderRequest renderRequest,
			RenderResponse renderResponse) throws IOException, PortletException {

		String redirect = PortalUtil.getCurrentURL(renderRequest);
		renderRequest.setAttribute("redirect", redirect);

		searchContainerData(renderRequest, renderResponse);

		PortletURL redirectURL = renderResponse.createActionURL();
		redirectURL.setParameter("javax.portlet.action", "doSearch");
		redirectURL.setParameters(renderRequest.getParameterMap());
		renderRequest.setAttribute("redirectURL", redirectURL);

		super.doView(renderRequest, renderResponse);
	}

	public void doSearch(ActionRequest actionRequest,
			ActionResponse actionResponse) throws IOException, PortletException {

		searchData(actionRequest, actionResponse);

		PortalUtil.copyRequestParameters(actionRequest, actionResponse);

		actionRequest.setAttribute("searchProcessed", 1);

		// para evitar duplicidade de submit
		sendRedirect(actionRequest, actionResponse);

	}

	private void searchData(PortletRequest request,
			PortletResponse response) {

		final ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);

		String searchExpression = ParamUtil.getString(request, "searchExpression");
		int delta = ParamUtil.getInteger(request,
				SearchContainer.DEFAULT_DELTA_PARAM,
				SearchContainer.DEFAULT_DELTA);
		int curPage = ParamUtil.getInteger(request,
				SearchContainer.DEFAULT_CUR_PARAM, SearchContainer.DEFAULT_CUR);
		int total = ParamUtil.getInteger(request, "searchTotal", 0);
		int start = (curPage - 1) * delta;
		int end = start + delta;
		if (end > total && total != 0) {
			end = total;
		}

		if (Validator.isNotNull(searchExpression)) {
			Query query = StringQueryFactoryUtil.create(searchExpression);

			if (_log.isDebugEnabled()) {
				_log.debug(query.toString());
			}

			SearchContext searchContext = new SearchContext();
			searchContext.setCompanyId(themeDisplay.getCompanyId());
			searchContext.setStart(start);
			searchContext.setEnd(end);

			request.setAttribute("searchResult", Collections.emptyList());
			request.setAttribute("searchTotal", 0);

			try {

				Hits hits = SearchEngineUtil.search(searchContext, query);

				if (hits != null) {
					if (_log.isDebugEnabled()) {
						_log.debug("hits: " + hits.getLength());
					}

					request.setAttribute("searchTotal", hits.getLength());
					if (hits.getLength() > 0) {
						request.setAttribute("searchResult", hits.toList());
					}
				}

			} catch (PortalException e) {
				SessionErrors.add(request, "search-error");
				request.setAttribute("searchError", e.getMessage());
				_log.error("search-error", e);
			}
		}
	}

	/**
	 *
	 * Preenche o SearchContainer
	 *
	 * @param renderRequest
	 * @param renderResponse
	 */
	public void searchContainerData(RenderRequest renderRequest,
			RenderResponse renderResponse) {

		ThemeDisplay themeDisplay = (ThemeDisplay) renderRequest.getAttribute(WebKeys.THEME_DISPLAY);

		/*
		 * Creating IteratorURL
		 */
		PortletURL iteratorURL = renderResponse.createActionURL();
		iteratorURL.setParameter("javax.portlet.action", "doSearch");
		iteratorURL.setParameter("searchExpression", ParamUtil.getString(renderRequest, "searchExpression"));

		/*
		 * Creating SearchContainer
		 */
		SearchContainer<Document> searchContainer = new SearchContainer<Document>(
				renderRequest, null, null, SearchContainer.DEFAULT_CUR_PARAM,
				ParamUtil.getInteger(renderRequest,
						SearchContainer.DEFAULT_DELTA_PARAM, SearchContainer.DEFAULT_DELTA), iteratorURL,
						null, LanguageUtil.format(themeDisplay.getLocale(),
								"no-entries-were-found", "No results"));

		searchData(renderRequest, renderResponse);

		//		int total = ParamUtil.get(renderRequest, "searchTotal", 0);
		//		searchContainer.setTotal(total);
		//
		//		@SuppressWarnings("unchecked")
		//		List<Document> searchResult = (List<Document>) renderRequest.getAttribute("searchResult");
		//		searchContainer.setResults(searchResult);

		renderRequest.setAttribute("searchContainer", searchContainer);
	}

	private Log _log = LogFactoryUtil.getLog(SearchIndexToolPortlet.class);
}
