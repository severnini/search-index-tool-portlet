<%@ include file="/html/searchindextool/init.jsp" %>

<liferay-portlet:actionURL var="searchURL" name="doSearch" />

<aui:layout>
	<div id="<portlet:namespace />tabResults">
	
	  <ul class="nav nav-tabs">
	  	<li <c:if test="${empty searchProcessed}"> class="active" </c:if>><a href="#<portlet:namespace />tab-query"><liferay-ui:message key="label-search-query" /></a></li>
	    <li <c:if test="${not empty searchProcessed}"> class="active" </c:if>><a href="#<portlet:namespace />tab-results"><liferay-ui:message key="label-search-result" /></a></li>
	  </ul>
	
	  <div class="tab-content">
	  	<div id="<portlet:namespace />tab-query" class="tab-pane <c:if test='${empty searchProcessed}'> active </c:if>">
				<!-- Search expression input -->
				<aui:layout>
					<aui:form action="${searchURL}" method="post" name="fm">
						<aui:input name="searchExpression" label="label-search-expression" value="${searchExpression}" type="textarea" style="width:90%;"/>
						<aui:button id="btnSearch" name="btnSearch" type="submit" value="btn-search"/>
					</aui:form>	
				</aui:layout>
	  	</div>
	  	
	    <div id="<portlet:namespace />tab-results" class="tab-pane <c:if test='${not empty searchProcessed}'> active </c:if>">				
			<liferay-ui:search-container searchContainer="${searchContainer}" total="${searchTotal}">
								
				<liferay-ui:search-container-results results="${searchResult}" />
										
				<liferay-ui:search-container-row className="com.liferay.portal.kernel.search.Document" modelVar="document" keyProperty="UID">
					<liferay-ui:search-container-column-text>
						<table>
							<thead>
								<tr>									
								<c:forEach items="${document.getFields().keySet()}" var="fieldKey">
									<td><strong>${fieldKey}</strong></td>
								</c:forEach>
								</tr>								
							</thead>
							<tbody>
								<tr>
								<c:forEach items="${document.getFields().keySet()}" var="fieldKey">
									<td> 
										<c:forEach items="${document.getValues(fieldKey)}" var="value" varStatus="status">
											<c:if test="${status.index != 0}"><br></c:if>
											${value}												
										</c:forEach>  
									</td>
								</c:forEach>									
								</tr>
							</tbody>
						</table>
					</liferay-ui:search-container-column-text>						
				</liferay-ui:search-container-row>
				
				<liferay-ui:search-iterator paginate="true" />
				
			</liferay-ui:search-container>								
	    </div>
	  </div>
	</div>		
</aui:layout>	

<liferay-ui:error key="search-error" message="${searchError}" />

<aui:script use="aui-tabview">
	tabView = new A.TabView(
			{
				srcNode: '#<portlet:namespace />tabResults'
			}
	);
	
	tabView.render();
	
	function enableTabResults() {
		tabView.enableTab(1);
	}	
</aui:script>